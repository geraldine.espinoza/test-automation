# new feature
# Tags: optional

Feature: FHIR Event Watch


  @fhirevent
  Scenario: Patient Event Creation, Verification and Deletion
    Given a rest api "FHIRCreateEvent"
    Given a header
      | Content-Type | application/json |
    And a request body "<<FHIREventWatch.default>>"
    When the system requests POST "/event/"
    And trace out request response
    Then the response code is 201
    And Extract and Save "id" from response

    #Get FHIR Event
    Given a rest api "FHIRCreateEvent"
    Given a header
      | Content-Type | application/json |
    When the system requests GET "/event/<id>"
    Then the response code is 200
    And trace out request response
    And verify response body contains
      | type           | Patient  |
    And verify response body contains
      | checks.resource| Observation   |
    And verify response body contains
      | checks.fhirpath| code.coding.where(system='http://loinc.org' and code='8480-6') and valueQuantity.where(value > 140 and unit='mmHg')|

    #Delete FHIR Event
    Given a rest api "FHIRCreateEvent"
    Given a header
      | Content-Type | application/json |
    When the system requests DELETE "/event/<id>"
    Then the response code is 200
    And trace out request response
    And validate response body contains "true"




