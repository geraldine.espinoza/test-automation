package hooks;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import automation.library.common.Property;
import automation.library.jira.core.JiraExecution;
import automation.library.reporting.Reporter;
import automation.library.selenium.exec.driver.factory.DriverFactory;
import io.cucumber.core.api.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;

public class Hooks {

    @AfterStep
    public void attachedScreenshot() {
        if (Boolean.parseBoolean(Property.getVariable("cukes.AllStepScreenShot"))) {
            String imgName = automation.library.selenium.core.Screenshot.saveScreenshot(automation.library.selenium.core.Screenshot.grabScrollingScreenshot(DriverFactory.getInstance().getDriver()), Reporter.getScreenshotPath()).getName();
            String relativePath = "." + File.separator + "Screenshots" + File.separator + imgName;
            //Reporter.addScreenCaptureFromPath(relativePath);
        }
    }

    @After
    public void addJiraReport(Scenario scenario) throws IOException, URISyntaxException, IllegalStateException{
        if (Boolean.parseBoolean(Property.getVariable("cukes.JiraReporter"))) {
            if(scenario.isFailed())
                automation.library.jira.core.JiraExecution.status = "2";
            JiraExecution.getTestExecutionRecord();
            if(Boolean.parseBoolean(Property.getVariable("cukes.JiraCloud"))) {
                JiraExecution.postExecutionRecordCreation();
                JiraExecution.putExecutionStatusUpdate();
            }
            JiraExecution.jiraAttachment();
        }
    }



//    @Before
//    public void startUp(Scenario scenario) throws IOException {
//    }

//    @After(order=40)
//    public void closeDown(Scenario scenario) throws InterruptedException {
//    }

//    @After(order=40)
//    public void closeDown2(Scenario scenario) throws InterruptedException {
//    }
}
