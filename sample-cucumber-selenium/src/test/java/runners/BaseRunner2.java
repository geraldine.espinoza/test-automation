package runners;

import automation.library.cucumber.api.RunnerClass;
import automation.library.cucumber.api.RunnerClassParallel;
import automation.library.reporting.TextReport;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.AfterTest;


@CucumberOptions(
       // plugin = {"automation.library.reporting.adapter.ExtentCucumberAdapter:"},
        plugin = {"io.qameta.allure.cucumber4jvm.AllureCucumber4Jvm","json:RunReports/cucumberJson/cucumber-report.json"},
        features = {"classpath:features"},
        glue = {"steps", "hooks"}

        )

public class BaseRunner2 extends RunnerClassParallel {

    //TestNG before hook
//    @BeforeTest
//    public static void setup() {
//        ExtentProperties extentProperties = ExtentProperties.INSTANCE;
//        extentProperties.setReportPath(Reporter.getReportPath() + Reporter.getReportName());
//    }

    //TestNG after hook
    @AfterTest
    public void teardown() {
        TextReport tr = new TextReport();
        tr.createReport(true);
    }
}