package pageobjects;

import automation.library.selenium.exec.BasePO;
import org.openqa.selenium.By;

public class PatientSummaryDetailsPage extends BasePO {
    public By remoteMonitoringDevicesLink = By.id("2");
    public By bloodPressureLink = By.xpath("//span[text()='Blood Pressure']");
    public By bloodPressure = By.xpath("//div[@class='widget-title-text']");
    public By vitalIcon = By.xpath("//i[@class='tab_list_head_icon icon_vital']");

    public void ViewBloodPressure(){
        $(remoteMonitoringDevicesLink).clickable().click();
        $(bloodPressureLink).clickable().click();
    }

    public void ViewVitals(){
        $(vitalIcon).clickable().click();
    }

    public String getText(){
        return $(bloodPressure).getText();
    }
}
