package pageobjects;

import automation.library.common.Encryption;
import automation.library.selenium.exec.BasePO;
import org.openqa.selenium.By;
import automation.library.common.Encryption;

import java.io.IOException;

public class PatientLoginPage extends BasePO {

    public By usernameField = By.id("okta-signin-username");
    public By passwordField = By.id("okta-signin-password");
    public By signInButton = By.id("okta-signin-submit");


    public PatientHomePage login() throws IOException {
        String username= automation.library.common.ConfigFileReader.obtainValue("environments/devtest","patientUserName");
        String password = automation.library.common.ConfigFileReader.obtainValue("environments/devtest","patientPassword");
        $(usernameField).sendKeys(username);
        password = automation.library.common.Encryption.decodeBase64(password);
        $(passwordField).sendKeys(password);
        $(signInButton).clickable().click();
        return new PatientHomePage();
    }


}
