package pageobjects;

import automation.library.selenium.exec.BasePO;
import org.openqa.selenium.By;

public class GoogleSearch extends BasePO {
    public By searchField = By.name("q");
    public By searchButton = By.name("btnK");

    public GoogleResults SearchItem(String searchTerm){
        $(searchField).sendKeys(searchTerm);
        $(searchButton).clickable().click();
        return new GoogleResults();
    }
}
