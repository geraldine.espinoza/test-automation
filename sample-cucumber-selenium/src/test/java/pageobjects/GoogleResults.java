package pageobjects;

import automation.library.selenium.exec.BasePO;
import org.openqa.selenium.By;

public class GoogleResults extends BasePO {

    public By searchResultLink = By.xpath("//*[@id='tvcap']/div/div/div/div[1]/h3/a/span");

    public String getText(){
        return $(searchResultLink).getText();
    }
}
