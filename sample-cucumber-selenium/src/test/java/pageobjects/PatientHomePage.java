package pageobjects;

import automation.library.selenium.exec.BasePO;
import org.openqa.selenium.By;

public class PatientHomePage extends BasePO {

    public By patientNameLink = By.xpath("//div[@class='piname' and text()='Patrick Taylor']");

    public PatientSummaryDetailsPage SelectPatient(){
        $(patientNameLink).clickable().click();
        return new PatientSummaryDetailsPage();
    }
}
