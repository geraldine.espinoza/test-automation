package steps;

import automation.library.cucumber.selenium.BaseSteps;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import pageobjects.GoogleResults;
import pageobjects.GoogleSearch;
import pageobjects.Journey;

public class GoogleSearchSteps extends BaseSteps {
    GoogleResults googleResults;

    @When("^user searches for \"(.*)\"$")
    public void SearchItem(String searchTerm)throws Throwable {
        GoogleSearch googleSearch = new GoogleSearch();
        //utils.TakeScreenshot();
        googleResults = googleSearch.SearchItem(searchTerm);
    }

    @Then("^\"(.*)\" is shown$")
    public void VerifySearchItem(String term)throws Throwable{
        //utils.TakeScreenshot();
        Assert.assertEquals(term,googleResults.getText());
    }
}
