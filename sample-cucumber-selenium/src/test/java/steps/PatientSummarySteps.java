package steps;

import automation.library.cucumber.selenium.BaseSteps;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import pageobjects.PatientHomePage;
import pageobjects.PatientLoginPage;
import pageobjects.PatientSummaryDetailsPage;

public class PatientSummarySteps extends BaseSteps {

    PatientHomePage patientHomePage = new PatientHomePage();
    PatientSummaryDetailsPage patientSummaryDetailsPage = new PatientSummaryDetailsPage();

    @When("^credentials are entered$")
    public void EnterCredentials()throws Throwable {
        //utils.TakeScreenshot();
        PatientLoginPage patientLoginPage = new PatientLoginPage();
        patientHomePage = patientLoginPage.login();
    }

    @When("^select patient$")
    public void SelectPatient() throws Throwable{
        //utils.TakeScreenshot();
        patientSummaryDetailsPage = patientHomePage.SelectPatient();
    }

    @When("^view the blood pressure readings$")
    public void ViewBloodPressure() throws Throwable{
        //utils.TakeScreenshot();
        patientSummaryDetailsPage.ViewBloodPressure();
    }

    @When("^view the vital readings$")
    public void ViewVitals() throws Throwable{
        //utils.TakeScreenshot();
        patientSummaryDetailsPage.ViewVitals();
    }

    @Then("^the \"(.*)\" readings are shown$")
    public void VerifyBloodPressure(String text) throws Throwable{
        //utils.TakeScreenshot();
        Assert.assertEquals(text,patientSummaryDetailsPage.getText());
    }
}
