package automation.library.jira.core;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import com.thed.zephyr.cloud.rest.ZFJCloudRestClient;
import com.thed.zephyr.cloud.rest.client.JwtGenerator;


public class Generator{

    public static String test(String endpoint, String requestType) throws URISyntaxException, IllegalStateException, IOException
    {
        // Replace Zephyr BaseUrl with the <ZAPI_CLOUD_URL> shared with ZAPI Cloud Installation
        String zephyrBaseUrl = "https://prod-api.zephyr4jiracloud.com/connect";
        // zephyr accessKey , we can get from Addons >> zapi section
        String accessKey = "MTg3NjM2MWYtNmQ2Yy0zZGUxLTk3M2EtMzFiN2Q5ZTVhZTA2IDVlNzg5ZTIxZTZhMWQzMGMzM2VlZTNjNCBVU0VSX0RFRkFVTFRfTkFNRQ";
        // zephyr secretKey , we can get from Addons >> zapi section
        String secretKey = "gT2-8Kp3M_j4eskSGoiW3a9c0IstIjZdY7_uECp8Mo0";
        // Jira accountId
        String userName = "5e789e21e6a1d30c33eee3c4";

        //String userName = "zephyr-support@smartbear.com";
        ZFJCloudRestClient client = ZFJCloudRestClient.restBuilder(zephyrBaseUrl, accessKey, secretKey, userName).build();
        JwtGenerator jwtGenerator = client.getJwtGenerator();

        // API to which the JWT token has to be generated
        String createCycleUri = zephyrBaseUrl + endpoint;
        URI uri = new URI(createCycleUri);
        int expirationInSec = 3600;
        System.out.println("request type:" + requestType);
        System.out.println("Uri: "+ uri.toString());
        String jwt = jwtGenerator.generateJWT(requestType, uri, expirationInSec);

        return jwt;

        // Print the URL and JWT token to be used for making the REST call
        //System.out.println(uri.toString());
        //System.out.println(jwt);

    }
}
