package automation.library.jira.core;

import automation.library.cucumber.selenium.CommonSteps;
import io.restassured.response.Response;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipOutputStream;
import java.util.zip.ZipEntry;

import static io.restassured.RestAssured.given;

public class JiraExecution {

    static String issueId;
    static String projectId;
    static String execId;
    public static String status = "1";
    static List<String> fileList;
    static final String OUTPUT_ZIP_FILE = "Report.zip";
    static final String SOURCE_FOLDER = System.getProperty("user.dir") + "/RunReports"; // SourceFolder path
    static String jiraUrl;
    static String jiraUser;
    static String jiraPassword;
    static String cycleId;
    static String versionId;
    static String zapiAccessKey;


    public static void getTestExecutionRecord() throws IOException, URISyntaxException, IllegalStateException{
        jiraUrl = automation.library.common.ConfigFileReader.obtainValue("jira/jira","jiraUrl");
        jiraUser = automation.library.common.ConfigFileReader.obtainValue("jira/jira","jiraUsername");
        jiraPassword =  automation.library.common.ConfigFileReader.obtainValue("jira/jira","jiraPassword");

        Response response = given().auth().preemptive().basic(jiraUser, jiraPassword).when().get(jiraUrl + "/rest/api/2/issue/" + CommonSteps.jiraId).then().extract().response();

        System.out.println("response: "+response.asString());

        issueId = response.jsonPath().getString("id");
        System.out.println("issue id : " + issueId);

        projectId = response.jsonPath().getString("fields.project.id");
        System.out.println("project id : " + projectId);
    }

    public static void postExecutionRecordCreation() throws IOException, URISyntaxException, IllegalStateException{
        String jwt = Generator.test("/public/rest/api/1.0/execution","POST");
        System.out.println("jwt: " + jwt);

        cycleId = automation.library.common.ConfigFileReader.obtainValue("jira/jira","jiraCycleId");
        versionId = automation.library.common.ConfigFileReader.obtainValue("jira/jira","jiraVersionId");
        zapiAccessKey = automation.library.common.ConfigFileReader.obtainValue("jira/jira","zapiAccessKey");

        String body = "{\"projectId\":" + projectId + ",\"issueId\":" + issueId + ",\"cycleId\":\"" + cycleId + "\",\"versionId\":" + versionId + "}";
        Response response = given().header("Content-Type", "application/json").header("Authorization", jwt).header("zapiAccessKey", zapiAccessKey).body(body).post("https://prod-api.zephyr4jiracloud.com/connect/public/rest/api/1.0/execution").then().extract().response();
        System.out.println("response: "+response.asString());
        execId = response.jsonPath().getString("execution.id");
        System.out.println("execution id: " + execId);
    }

    public static void putExecutionStatusUpdate() throws IOException, URISyntaxException, IllegalStateException{
        String jwt = Generator.test("/public/rest/api/1.0/execution/" + execId,"PUT");
        System.out.println("jwt: " + jwt);

        System.out.println("status: " + status);

        String body = "{\"status\":{\"id\":" + status + "},\"projectId\":" + projectId + ",\"issueId\":\"" + issueId + "\",\"cycleId\":\"" + cycleId + "\",\"versionId\":\"" + versionId + "\"}";

        System.out.println("body:" + body);

        Response response = given().header("Content-Type", "application/json").header("Authorization", jwt).header("zapiAccessKey", zapiAccessKey).body(body).put("https://prod-api.zephyr4jiracloud.com/connect/public/rest/api/1.0/execution/" + execId).then().extract().response();
        System.out.println("response: "+response.asString());
    }

    public static void jiraAttachment() throws ClientProtocolException, IOException {
        fileList = new ArrayList<String>();
        generateFileList(new File(SOURCE_FOLDER));
        zipFile(OUTPUT_ZIP_FILE);

        String auth = new String(org.apache.commons.codec.binary.Base64.encodeBase64((jiraUser+":"+jiraPassword).getBytes()));


        HttpClient httpclient = new DefaultHttpClient();
        System.out.println("jirapost url "+ jiraUrl + "/rest/api/2/issue/" + CommonSteps.jiraId + "/attachments");
        HttpPost httppost = new HttpPost(jiraUrl + "/rest/api/2/issue/" + CommonSteps.jiraId + "/attachments");
        httppost.setHeader("X-Atlassian-Token", "nocheck");
        httppost.setHeader("Authorization", "Basic "+auth);
        MultipartEntity entity = new MultipartEntity();

        File fileToUpload = new File(System.getProperty("user.dir") + "/Report.zip");
        FileBody fileBody = new FileBody(fileToUpload);
        entity.addPart("file", fileBody);

        httppost.setEntity(entity);
        HttpResponse response = null;
        try {
            response = httpclient.execute(httppost);
        } catch (ClientProtocolException e) {

        } catch (IOException e) {
        }
    }


    public static void zipFile(String zipFile) throws IOException {
        byte[] buffer = new byte[1024];
        String source = new File(SOURCE_FOLDER).getName();
        FileOutputStream fos = null;
        ZipOutputStream zos = null;

        try {
            fos = new FileOutputStream(zipFile);
            zos = new ZipOutputStream(fos);

            FileInputStream in = null;

            for (String file : fileList) {
                ZipEntry ze = new ZipEntry(source + File.separator + file);
                zos.putNextEntry(ze);
                try {
                    in = new FileInputStream(SOURCE_FOLDER + File.separator + file);
                    int len;
                    while ((len = in.read(buffer)) > 0) {
                        zos.write(buffer, 0, len);
                    }
                } finally {
                    in.close();
                }
            }
            zos.closeEntry();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                zos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public static void generateFileList(File node) {
        // add file only
        if (node.isFile()) {
            fileList.add(node.toString().substring(SOURCE_FOLDER.length() + 1, node.toString().length()));
        }

        if (node.isDirectory()) {
            String[] subNote = node.list();
            for (String filename : subNote) {
                generateFileList(new File(node, filename));
            }
        }
    }
}



