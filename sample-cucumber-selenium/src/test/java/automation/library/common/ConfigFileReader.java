package automation.library.common;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigFileReader {

    public static String obtainValue(String file, String propertyValueName) throws IOException {
        Properties prop = new Properties();
        String fileName = System.getProperty("user.dir") + "/src/test/resources/config/" + file + ".properties";
        InputStream is = null;
        try {
            is = new FileInputStream(fileName);
            prop.load(is);
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        }
        return prop.getProperty(propertyValueName);
    }
}
