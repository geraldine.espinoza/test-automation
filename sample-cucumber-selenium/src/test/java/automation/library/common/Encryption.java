package automation.library.common;

import org.apache.commons.codec.binary.Base64;

import java.util.Arrays;

public class Encryption {

    public static  String decodeBase64(String password){
        password = password.replace("Enc(","");
        password = password.replace(")","");

        byte[] decodedBytes = Base64.decodeBase64(password);
        String decodedString = new String(decodedBytes);

        return decodedString;

    }

}
