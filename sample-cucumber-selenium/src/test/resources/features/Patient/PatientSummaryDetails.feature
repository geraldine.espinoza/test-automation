# new feature
# Tags: optional

Feature: Patient Summary Details

  @patient
  Scenario: View Patient Blood Pressure Details
    Given the application "PatientSummary"
    And the jira is "ICI-969"
    When credentials are entered
    And select patient
    And view the blood pressure readings
    Then the "Blood Pressure" readings are shown

  #@patient
  Scenario: View Patient Medical Summary Details
   Given the application "PatientSummary"
    And the jira is "ICI-1052"
   When credentials are entered
   And select patient
    And view the vital readings
   Then the "VITAL" readings are shown

