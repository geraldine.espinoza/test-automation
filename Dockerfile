FROM ubuntu:latest

MAINTAINER Timofey Pirogov "tpirogov@accenture.com"

ENV LANG C.UTF-8
ENV DEBIAN_FRONTEND noninteractive
ENV DEBCONF_NONINTERACTIVE_SEEN true


ENV DOCKER_USER=ubuntu
ENV ACCTESTFW=afw

USER root
RUN echo "America/New_York" > /etc/timezone
# RUN dpkg-reconfigure -f noninteractive tzdata

RUN	echo '---------------------------------------' && \
	echo 'current date' && date && \
	echo 'current date' && date && \
	echo '---------------------------------------' && \
	mkdir -p /home/$DOCKER_USER && \
	echo 'Installing OS dependencies' && \
	apt-get update && \
	echo 'Installing fix-missing software-properties-common' && \
    apt-get install -qq -y --fix-missing software-properties-common 
RUN apt-get clean
RUN	echo '---------------------------------------' && \
    echo '------------Installing sudo ------------' && \
    echo '---------------------------------------' && \
	apt-get install sudo							
RUN echo '---------------------------------------' && \
	echo '------------installing bash        ---' && \
	echo '---------------------------------------' && \
	apt install bash 								
	
RUN echo '---------------------------------------' && \
    echo '------------installing Gradle        ---' && \
    echo '---------------------------------------' && \
	sudo apt-get install -y gradle 					

RUN	echo '---------------------------------------' && \
    echo '------------Installing curl ------------' && \
    echo '---------------------------------------' && \
	sudo apt-get install -y curl
	
RUN echo '---------------------------------------' && \
    echo '------------Installing wget ------------' && \
    echo '---------------------------------------' && \
    sudo apt-get install -y wget					
	
RUN echo '---------------------------------------' && \
    echo '------------Installing maven ------------' && \
    echo '---------------------------------------' && \
    sudo apt-get  install -y maven
RUN apt-get clean
RUN echo '---------------------------------------' && \
    echo '------------Installing Google Chrome --' && \
    echo '---------------------------------------' 
	
RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb && \
    apt install -y ./google-chrome-stable_current_amd64.deb && \
	echo '---------------------------------------' && \
    echo '------------Installing unzip ----------' && \
    echo '---------------------------------------' && \
	sudo apt-get install -y unzip						&& \
	echo '---------------------------------------' && \
    echo '------------Installing chromedriver ---' && \
    echo '---------------------------------------' && \
    cd /tmp && \
    wget -N https://chromedriver.storage.googleapis.com/78.0.3904.70/chromedriver_linux64.zip -P /tmp && \
    unzip chromedriver_linux64.zip && \
	rm chromedriver_linux64.zip && \
    mv -f chromedriver /usr/bin/chromedriver && \
	chown root:root /usr/bin/chromedriver && \
	chmod 0755 /usr/bin/chromedriver 
	
RUN	echo '---------------------------------------' && \
    echo '------------installing Git        ---' && \
    echo '---------------------------------------' && \
	sudo apt-get install -y git						&& \
	echo 'Update ...' && \
	apt-get upgrade -y -q && \
    echo 'Cleaning up' && \
    apt-get clean -q -y && \
    apt-get autoclean -q -y && \
    apt-get autoremove -q -y &&  \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /tmp/* && \
	echo '---------------------------------------' && \
    echo '------------Installing JDK 11       ---' && \
    echo '---------------------------------------' && \ 
	wget https://download.java.net/java/GA/jdk11/9/GPL/openjdk-11.0.2_linux-x64_bin.tar.gz -O /tmp/openjdk-11.0.2_linux-x64_bin.tar.gz && \ 
	sudo tar xfvz /tmp/openjdk-11.0.2_linux-x64_bin.tar.gz --directory /usr/lib/jvm && \
	sudo sh -c 'for bin in /usr/lib/jvm/jdk-11.0.2/bin/*; do update-alternatives --install /usr/bin/$(basename $bin) $(basename $bin) $bin 100; done' && \
	sudo sh -c 'for bin in /usr/lib/jvm/jdk-11.0.2/bin/*; do update-alternatives --set $(basename $bin) $bin; done'
	
#RUN	echo '---------------------------------------' && \
#    echo '------------installing firefox         ---' && \
#    echo '---------------------------------------' && \
#	sudo apt-get install -y firefox

RUN	echo '---------------------------------------' && \
    echo '------------driver versions         ---' && \
    echo '---------------------------------------' && \
    java -version && \
    mvn -v && \
    google-chrome --version && \
#	firefox -version && \
    chromedriver -v && \
	gradle --version
	
RUN echo 'Creating user: $DOCKER_USER' && \
    mkdir -p /home/$DOCKER_USER && \
    echo "$DOCKER_USER:x:1000:1000:$DOCKER_USER,,,:/home/$DOCKER_USER:/bin/bash" >> /etc/passwd && \
    echo "$DOCKER_USER:x:1000:" >> /etc/group && \
    sudo echo "$DOCKER_USER ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/$DOCKER_USER && \
    sudo chmod 0440 /etc/sudoers.d/$DOCKER_USER && \
    sudo chown $DOCKER_USER:$DOCKER_USER -R /home/$DOCKER_USER && \
    sudo chown root:root /usr/bin/sudo && \
    chmod 4755 /usr/bin/sudo
	

RUN echo '---------------------------------------------------------------------------' && \
    echo '------------Hold for several minutes, we are updating files permisions  ---' && \
    echo '---------------------------------------------------------------------------' && \
	sudo chown $DOCKER_USER -R /home/$DOCKER_USER
ADD sample-cucumber-selenium /home/$DOCKER_USER/atomclone/sample-cucumber-selenium
RUN ls /home/ubuntu
RUN sudo chmod 0755  /home/$DOCKER_USER/atomclone/sample-cucumber-selenium/lib/drivers/linux/chromedriver 
